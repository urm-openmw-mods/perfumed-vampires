return {
    gen_target = '5.1',
    gen_compat = 'off',
    include_dir = {
        'src',
        '../teal_declarations',
    },
    source_dir = 'src',
    build_dir = 'Data Files',
}
