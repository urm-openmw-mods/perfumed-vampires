# Perfumed Vampires

"Perfumed Vampires" allows the player to mask their undead scent by applying perfumes. The effect lasts as long as the perfume itself.

By default, Telvanni Bug Musk and Project Cyrodiil perfumes are supported.

Requires a recent [development build of OpenMW](https://openmw.org/downloads/).

To install, read the documentation on
[how to install OpenMW mods](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html?highlight=installing%20mods)
and [how to enable OpenMW scripts](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#how-to-run-a-script)

![](banner.png)

## API

Global interface `urm_PerfumedVampires`:

* `version`: `1`
* `registerPerfume(recordId)`: treats potion with `id` as a perfume
* `isPerfumed(actor)`: returns `true` if the given actor (player) has an active perfume
* `isVampire(actor)`: returns `true` if the given actor (player) is a vampire, whether perfumed or not
