local core = require('openmw.core')
local world = require('openmw.world')
local types = require('openmw.types')
local async = require('openmw.async')
local I = require('openmw.interfaces')

local PERFUME_ID_SET: {string: boolean} = {
    ['potion_t_bug_musk_01'] = true,
}

-- perfumes from TR Data
for i = 1, 6 do
    PERFUME_ID_SET[('t_com_subst_perfume_%02d'):format(i)] = true
end

local function maxEffectDuration(rec: types.PotionRecord): number
    local duration: number = 0
    for _, effect in ipairs(rec.effects) do
        if effect.duration > duration then
            duration = effect.duration
        end
    end
    return duration
end

local function hasVampirism(player: core.GameObject): boolean
    local globals = world.mwscript.getGlobalVariables(player)
    return globals.pcvampire ~= -1 and types.Actor.activeEffects(player):getEffect('vampirism').magnitudeModifier > 0
end

local function setVampirism(player: core.GameObject, active: boolean)
    if not hasVampirism(player) then return end
    types.Actor.activeEffects(player):set(active and 1 or 0, 'vampirism')
    local globals = world.mwscript.getGlobalVariables(player)
    globals.pcvampire = active and 1 or 0
end

local actorPefumes: {string: number} = {}

local perfumeRanOut = async:registerTimerCallback('urm_PerfumedVampires_perfumeRanOut', function(player: core.GameObject)
    local count = (actorPefumes[player.id] or 0) - 1
    if count <= 0 then
        setVampirism(player, true)
        actorPefumes[player.id] = nil
    end
end)

I.ItemUsage.addHandlerForType(types.Potion, function(potion: core.GameObject, actor: core.GameObject)
    if actor.type ~= types.Player then return end
    if not PERFUME_ID_SET[potion.recordId] then return end
    if not hasVampirism(actor) then return end

    actorPefumes[actor.id] = (actorPefumes[actor.id] or 0) + 1
    setVampirism(actor, false)
    local potionRecord = types.Potion.record(potion)
    local duration = maxEffectDuration(potionRecord)
    async:newGameTimer(duration * core.getGameTimeScale(), perfumeRanOut, actor)
end)

local record Saved
    actorPefumes: {string: number}
end

return {
    interfaceName = 'urm_PerfumedVampires',
    interface = {
        version = 1,
        registerPerfume = function(id: string)
            PERFUME_ID_SET[id] = true
        end,
        isPerfumed = function(actor: core.GameObject): boolean
            return (actorPefumes[actor.id] or 0) > 0
        end,
        isVampire = function(actor: core.GameObject): boolean
            return hasVampirism(actor)
        end,
        onSave = function(): Saved
            return {
                actorPefumes = actorPefumes,
            }
        end,
        onLoad = function(saved: Saved)
            if not saved then return end
            actorPefumes = saved.actorPefumes
        end,
    }
}
